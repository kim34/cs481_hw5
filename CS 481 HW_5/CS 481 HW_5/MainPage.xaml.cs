﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Essentials;
using Xamarin.Forms.Maps;


namespace CS_481_HW_5
{
   
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
       
        public MainPage()
        {
            InitializeComponent();

            var initialLocation = MapSpan.FromCenterAndRadius(new Position(33.1307785, -117.1601826), Distance.FromMiles(1));

            map.MoveToRegion(initialLocation);
        }

        void OnSliderValueChanged(object sender, ValueChangedEventArgs e)
        {
            double zoomLevel = e.NewValue;
            double latlongDegrees = 360 / (Math.Pow(2, zoomLevel));
            if (map.VisibleRegion != null)
            {
                map.MoveToRegion(new MapSpan(map.VisibleRegion.Center, latlongDegrees, latlongDegrees));
            }
        }

        void OnButtonClicked(object sender, EventArgs e)
        {
            Button button = sender as Button;
            switch (button.Text)
            {
                case "Street":
                    map.MapType = MapType.Street;
                    break;

                case "Satellite":
                    map.MapType = MapType.Satellite;
                    break;

                case "Hybrid":
                    map.MapType = MapType.Hybrid;
                    break;
            }
        }

        void PinClicked(object sender, EventArgs e)
        {
            Pin Sandiego = new Pin
            {
                Position = new Position(32.695855, -117.189502),
                Label = "San diego",
                Address = "San diego",
                Type = PinType.Place
            };
               

            Pin SouthKorea = new Pin
            {
                Position = new Position(37.428808, 126.863228),
                Label = "SouthKorea",
                Address = "SouthKorea",
                Type = PinType.Place
            };


            Pin LA = new Pin
            {
                Position = new Position(34.031720, -118.245610),
                Label = "LA",
                Address = "LA",
                Type = PinType.Place
            };

            Pin JejuIIsland = new Pin
            {
                Position = new Position(33.397501, 126.546834),
                Label = "JejuIIsland",
                Address = "JejuIIsland",
                Type = PinType.Place
            };

            var listData = new List<string>();

            map.Pins.Add(Sandiego);
            map.Pins.Add(SouthKorea);
            map.Pins.Add(LA);
            map.Pins.Add(JejuIIsland);

            
        }




    }
}
